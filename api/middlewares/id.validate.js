var joi = require('joi');

const idValidate = (req, res, next) => {
  
  var schema = joi.object({
    id: joi.string().uuid(),
  }).required();
  
  var isValidResult = schema.validate(req.params);
  
  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  };
  
  next();
  
}

module.exports = idValidate;