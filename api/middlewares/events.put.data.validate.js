var joi = require('joi');

const eventsPutDataValidate = (req, res, next) => {

  var schema = joi.object({
    score: joi.string().required(),
  }).required();

  var isValidResult = schema.validate(req.body);

  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  };  

  next();

}

module.exports = eventsPutDataValidate;