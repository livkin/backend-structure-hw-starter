
var jwt = require("jsonwebtoken");

const betsPostAuthorization = (req, res, next) => {

  let token = req.headers['authorization'];
  if (!token) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
  token = token.replace('Bearer ', '');
  try {
    var tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    req.userId = tokenPayload.id; //TODO - check id outside
  } catch (err) {
    console.log(err);
    return res.status(401).send({ error: 'Not Authorized' });
  }

  next();

}

module.exports = betsPostAuthorization;