var dbConfig = require("../../knexfile");
var knex = require("knex");

var healthRoutes = require('./health.routes');
var usersIdRouter = require('./users.router');
var transactionsRouter = require('./transactions.routes');
var eventsRouter = require('./events.routes');
var betsRouter = require('./bets.routes');
var statsRouter = require('./stats.routes');


const routes = (app) => {

  var db = knex(dbConfig.development);

  app.use("/health", healthRoutes);
  app.use("/users", usersIdRouter);
  app.use("/transactions", transactionsRouter);
  app.use("/events", eventsRouter);
  app.use("/bets", betsRouter);
  app.use("/stats", statsRouter);

};

module.exports = routes;