var joi = require('joi');
var dbConfig = require("../../knexfile");
var knex = require("knex");
var jwt = require("jsonwebtoken");

var statEmitter = require('../../helpers/statEmitter').getInstance();

const router = require("express").Router();

var db = knex(dbConfig.development);

var stats = require('../../helpers/stats').getInstance();
const statsGetAuthorization = require('../middlewares/authorization/stats.get.authorization');

router.get("/", statsGetAuthorization, (req, res) => {
  try {
    res.send(stats);
  } catch (err) {
    console.log(err);
    res.status(500).send("Internal Server Error");
    return;
  }
});

module.exports = router;