var joi = require('joi');
var dbConfig = require("../../knexfile");
var knex = require("knex");
var jwt = require("jsonwebtoken");

// var statEmitter = require('../../helpers/statEmitter').getInstance();

const router = require("express").Router();
const transactionsPostDataValidate = require('../middlewares/transactions.post.data.validate');
const transactionPostAuthorization = require('../middlewares/authorization/transaction.post.authorization');


var db = knex(dbConfig.development);

router.post("/", transactionPostAuthorization, transactionsPostDataValidate, (req, res) => {

  db("user").where('id', req.body.userId).then(([user]) => {

    if (!user) {
      res.status(400).send({ error: 'User does not exist' });
      return;
    }

    req.body.card_number = req.body.cardNumber;
    delete req.body.cardNumber;
    req.body.user_id = req.body.userId;
    delete req.body.userId;

    db("transaction").insert(req.body).returning("*").then(([result]) => {

      var currentBalance = req.body.amount + user.balance;

      db("user").where('id', req.body.user_id).update('balance', currentBalance).then(() => {

        ['user_id', 'card_number', 'created_at', 'updated_at'].forEach(whatakey => {

          var index = whatakey.indexOf('_');
          var newKey = whatakey.replace('_', '');
          newKey = newKey.split('')
          newKey[index] = newKey[index].toUpperCase();
          newKey = newKey.join('');
          result[newKey] = result[whatakey];
          delete result[whatakey];

        })

        return res.send({
          ...result,
          currentBalance,
        });

      });
    });

  }).catch(err => {
    res.status(500).send("Internal Server Error");
    return;
  });

});

module.exports = router;