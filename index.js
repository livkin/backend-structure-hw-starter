var express = require("express");
var knex = require("knex");
var jwt = require("jsonwebtoken");
var joi = require('joi');
var ee = require('events');
var dbConfig = require("./knexfile");

var routes = require('./api/routes/index');

//////////////////////////

var app = express();

var port = 3000;

// var statEmitter = new ee();
var statEmitter = require('./helpers/statEmitter').getInstance();

// var stats = {
//   totalUsers: 3,
//   totalBets: 1,
//   totalEvents: 1,
// };

var stats = require('./helpers/stats').getInstance();

var db;

////////////////////////////

app.use(express.json());

//middleware
app.use((uselessRequest, uselessResponse, neededNext) => {
  db = knex(dbConfig.development);
  db.raw('select 1+1 as result').then(function () {
    neededNext();
  }).catch(() => {
    throw new Error('No db connection');
  });
});

////////////////////////////

//api

routes(app);

/////////////////////////////////////////////////

app.listen(port, () => {
  statEmitter.on('newUser', () => {
    stats.totalUsers++;
  });
  statEmitter.on('newBet', () => {
    stats.totalBets++;
  });
  statEmitter.on('newEvent', () => {
    stats.totalEvents++;
  });

  console.log(`App listening at http://localhost:${port}`);
});

/////////////////////////////////////////////////

// Do not change this line
module.exports = { app };