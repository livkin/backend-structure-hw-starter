const EventEmitter = require('events');
//var ee = require('events');

let instance;

module.exports = {
  getInstance: function() {
    return instance || (instance = new EventEmitter())
  }
}
