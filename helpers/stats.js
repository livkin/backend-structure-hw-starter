
let instance;

module.exports = {
  getInstance: function() {
    return instance || (instance = {
        totalUsers: 3,
        totalBets: 1,
        totalEvents: 1,
    })
  }
}
